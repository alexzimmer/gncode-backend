package main;

import javax.json.JsonArray;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import main.dbaccess.DBAccess;

@Path("root")
public class BaseResource {

	@GET
	public String root() {
		return "It's working";
	}

	@Path("location")
	@GET
	public JsonArray resultsForLocation(@QueryParam("loc") String location) {
		JsonArray ja = DBAccess.locations(location);
		return ja;
	}
}
