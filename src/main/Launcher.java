package main;

import java.io.IOException;
import java.net.URI;

import javax.ws.rs.core.UriBuilder;

import org.glassfish.jersey.jdkhttp.JdkHttpServerFactory;
import org.glassfish.jersey.server.ResourceConfig;

import com.sun.net.httpserver.HttpServer;

public class Launcher {

	public static void main(String... args) throws IOException {
		URI baseUri = UriBuilder.fromUri("http://localhost/").port(80).build();
		ResourceConfig config = new ResourceConfig(BaseResource.class);
		HttpServer server = JdkHttpServerFactory.createHttpServer(baseUri, config);

		System.in.read();
		server.stop(0);
	}
}
