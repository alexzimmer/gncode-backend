package main;

import java.util.List;
import java.util.function.Function;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonValue;

public class JsonUtil {

	public static <T> JsonArray jsonArray(List<?> list, Function<T, JsonValue> func) {
		JsonArrayBuilder jab = Json.createArrayBuilder();

		list.stream()
				.map(e -> (T) e)
				.map(func)
				.forEach(jab::add);

		return jab.build();
	}
}
