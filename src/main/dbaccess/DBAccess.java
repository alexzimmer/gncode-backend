package main.dbaccess;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class DBAccess {

	private static final WebTarget ROOT = ClientBuilder.newBuilder()
			.build()
			.target("https://api.deutschebahn.com/freeplan/v1/");

	public static JsonArray locations(String fragments) {
		Response r = ROOT.path("location")
				.path(fragments)
				.request(HttpHeaders.ACCEPT, MediaType.APPLICATION_JSON)
				.header(HttpHeaders.AUTHORIZATION, "Bearer 404132f80081cf037b71964febe7b07a")
				.get();

		JsonArrayBuilder jb = Json.createArrayBuilder();

		r.readEntity(JsonArray.class)
				.stream()
				.map(JsonObject.class::cast)
				.map(e -> Json.createObjectBuilder()
						.add("name", e.getString("name"))
						.add("id", e.getInt("id"))
						.build())
				.forEach(jb::add);

		return jb.build();
	}

}
